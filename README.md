# TP4 et TP5

##TP5 Valgrind output

valgrind --leak-check=full ./unittests

==3743== HEAP SUMMARY:

==3743==     in use at exit: 0 bytes in 0 blocks

==3743==   total heap usage: 4,594,080 allocs, 4,594,080 frees, 1,606,035,452 bytes allocated

==3743==

==3743== All heap blocks were freed -- no leaks are possible

==3743==

==3743== For lists of detected and suppressed errors, rerun with: -s

==3743== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)

##TP5 Before optimisation

Finished addition (1000) in avg. 8ms, total 8518ms

Finished addition (3000) in avg. 58ms, total 2929ms

Finished substraction (1000) in avg. 6ms, total 6363ms

Finished substraction (3000) in avg. 58ms, total 2938ms

Finished multiplication (100) in avg. 1ms, total 134ms

Finished multiplication (500) in avg. 218ms, total 654ms

Finished multiplication (1000) in avg. 2972ms, total 8918ms

Finished multiplication (2000) in avg. 80807ms, total 80807ms

##TP5 After optimization

Finished addition (1000) in avg. 6ms, total 6597ms

Finished addition (3000) in avg. 59ms, total 2998ms

Finished substraction (1000) in avg. 6ms, total 6605ms

Finished substraction (3000) in avg. 60ms, total 3000ms

Finished multiplication (100) in avg. 0ms, total 16ms

Finished multiplication (500) in avg. 15ms, total 45ms

Finished multiplication (1000) in avg. 128ms, total 386ms

Finished multiplication (2000) in avg. 1765ms, total 1765ms