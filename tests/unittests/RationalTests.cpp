#include "../../Catch2/catch.hpp"
#include <string>
#include <sstream>
#include "../../core/rational/rat.h"

TEST_CASE("Tests constructor Rat(int num, int den)", "[constructor]"){

    SECTION("Rational positive numerator only"){
        Rat rat_a(3);
        REQUIRE(rat_a.getNumerator() == 3);
        REQUIRE(rat_a.getDenominator() == 1);
    }

    SECTION("Rational negative numerator only"){
        Rat rat_a(-3);
        REQUIRE(rat_a.getNumerator() == -3);
        REQUIRE(rat_a.getDenominator() == 1);
    }

    SECTION("Rational positive numerator only"){
        Rat rat_a(3);
        REQUIRE(rat_a.getNumerator() == 3);
    }

    SECTION("Rational positive numerator and positive denominator"){
        Rat rat_a(1,2);
        REQUIRE(rat_a.getNumerator() == 1);
        REQUIRE(rat_a.getDenominator() == 2);
    }

    SECTION("Rational negative numerator and positive denominator"){
        Rat rat_a(-1,2);
        REQUIRE(rat_a.getNumerator() == -1);
        REQUIRE(rat_a.getDenominator() == 2);
    }

    SECTION("Rational positive numerator and negative denominator"){
        Rat rat_a(1,-2);
        REQUIRE(rat_a.getNumerator() == -1);
        REQUIRE(rat_a.getDenominator() == 2);
    }

    SECTION("Rational negative numerator and negative denominator"){
        Rat rat_a(-1,-2);
        REQUIRE(rat_a.getNumerator() == 1);
        REQUIRE(rat_a.getDenominator() == 2);
    }

    SECTION("Rational numerator zero and denominator zero"){
        Rat rat_a(0,0);
        REQUIRE(rat_a.getNumerator() == 0);
        REQUIRE(rat_a.getDenominator() == 1);
    }

    SECTION("Rational denominator zero"){
        CHECK_THROWS_AS(Rat(1,0), std::domain_error);
        REQUIRE_THROWS(Rat(1,0));
    }

    SECTION("Rational numerator only"){
        Rat rat_a(2);
        REQUIRE(rat_a.getNumerator() == 2);
        REQUIRE(rat_a.getDenominator() == 1);
    }

}

TEST_CASE("Tests constructor Rat(double value, int accuracy"){
    SECTION("Positive values"){
        Rat rat_a(4.250,0);
        REQUIRE(rat_a.getNumerator() == 4);
        REQUIRE(rat_a.getDenominator() == 1);

        Rat rat_b(4.250,1);
        REQUIRE(rat_b.getNumerator() == 21);
        REQUIRE(rat_b.getDenominator() == 5);

        Rat rat_c(4.250,2);
        REQUIRE(rat_c.getNumerator() == 17);
        REQUIRE(rat_c.getDenominator() == 4);
    }

    SECTION("Negative values"){
        Rat rat_a(-4.250,0);
        REQUIRE(rat_a.getNumerator() == -4);
        REQUIRE(rat_a.getDenominator() == 1);

        Rat rat_b(-4.250,1);
        REQUIRE(rat_b.getNumerator() == -21);
        REQUIRE(rat_b.getDenominator() == 5);

        Rat rat_c(-4.250,2);
        REQUIRE(rat_c.getNumerator() == -17);
        REQUIRE(rat_c.getDenominator() == 4);
    }

    SECTION("Negative accuracy"){
        REQUIRE_THROWS(Rat(-4.250, -1));
    }

    SECTION("Value zero"){
        Rat rat_a(0.0,0);
        REQUIRE(rat_a.getNumerator() == 0);
        REQUIRE(rat_a.getDenominator() == 1);

        Rat rat_b(0,1);
        REQUIRE(rat_b.getNumerator() == 0);
        REQUIRE(rat_b.getDenominator() == 1);
    }
}

TEST_CASE("other methods", "[methods]"){
    SECTION("Function Rat::simplifyRational"){
        Rat rat_a(5,10);
        REQUIRE(rat_a.getNumerator() == 1);
        REQUIRE(rat_a.getDenominator() == 2);

        Rat rat_b(20,2);
        REQUIRE(rat_b.getNumerator() == 10);
        REQUIRE(rat_b.getDenominator() == 1);

        Rat rat_c(-5,10);
        REQUIRE(rat_c.getNumerator() == -1);
        REQUIRE(rat_c.getDenominator() == 2);

        Rat rat_d(5,-10);
        REQUIRE(rat_d.getNumerator() == -1);
        REQUIRE(rat_d.getDenominator() == 2);
    }

    SECTION("Function Rat::adjustSign()"){
        Rat rat_a(-4,3);
        REQUIRE(rat_a.getNumerator() == -4);
        REQUIRE(rat_a.getDenominator() == 3);

        Rat rat_b(-4,-3);
        REQUIRE(rat_b.getNumerator() == 4);
        REQUIRE(rat_b.getDenominator() == 3);

        Rat rat_c(4,-3);
        REQUIRE(rat_c.getNumerator() == -4);
        REQUIRE(rat_c.getDenominator() == 3);

        Rat rat_d(4,3);
        REQUIRE(rat_d.getNumerator() == 4);
        REQUIRE(rat_d.getDenominator() == 3);
    }

    SECTION("Function  Rat::toFloat()"){
        REQUIRE(Rat(0,0).toFloat() == 0.0f);
        REQUIRE(Rat(1,2).toFloat() == 0.5f);
        REQUIRE(Rat(-1,2).toFloat() == -0.5f);
        REQUIRE(Rat(1,4).toFloat() == 0.25f);
    }

    SECTION("Transtypage"){
        Rat rat_a = (Rat)4.250;

        REQUIRE(rat_a.getNumerator() == 17);
        REQUIRE(rat_a.getDenominator() == 4);

        double double_rat_a = (double)rat_a;
        REQUIRE(double_rat_a == 4.250);

        Rat rat_b = (Rat)-4.250;
        double double_rat_b = (double)rat_b;
        REQUIRE(double_rat_b == -4.250);

        Rat rat_c = (Rat)0.0;
        double double_rat_c = (double)rat_c;
        REQUIRE(double_rat_c == 0.0);

        Rat rat_d = Rat(4.250);
        double double_rat_d = double(rat_d);
        REQUIRE(double_rat_d == 4.250);

        Rat rat_e = Rat(4.250);
        double double_rat_e = static_cast<double>(rat_e);
        REQUIRE(double_rat_e == 4.250);

        Rat rat_f = static_cast<Rat>(4.250);
        double double_rat_f = static_cast<double>(rat_f);
        REQUIRE(double_rat_f == 4.250);

    }
}

TEST_CASE("operators", "[operators"){
    SECTION("operator +my_rat"){
        Rat rat_a(4,3);
        +rat_a;
        REQUIRE(rat_a.getNumerator() == 4);
        REQUIRE(rat_a.getDenominator() == 3);

        Rat rat_b(-4,3);
        +rat_b;
        REQUIRE(rat_b.getNumerator() == -4);
        REQUIRE(rat_b.getDenominator() == 3);

        Rat rat_c(0,0);
        +rat_c;
        REQUIRE(rat_c.getNumerator() == 0);
        REQUIRE(rat_c.getDenominator() == 1);
    }

    SECTION("operator -my_rat"){
        Rat rat_a(4,3);
        rat_a = -rat_a;
        REQUIRE(rat_a.getNumerator() == -4);
        REQUIRE(rat_a.getDenominator() == 3);

        Rat rat_b(-4,3);
        rat_b = -rat_b;
        REQUIRE(rat_b.getNumerator() == 4);
        REQUIRE(rat_b.getDenominator() == 3);

        Rat rat_c(0,0);
        +rat_c;
        REQUIRE(rat_c.getNumerator() == 0);
        REQUIRE(rat_c.getDenominator() == 1);
    }

    SECTION("operator rat_a + rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(-1,4);
        Rat rat_e(0,0);

        Rat sum_ab = rat_a + rat_b;
        REQUIRE(sum_ab.getNumerator() == 3);
        REQUIRE(sum_ab.getDenominator() == 4);

        Rat sum_ac = rat_a + rat_c;
        REQUIRE(sum_ac.getNumerator() == 0);
        REQUIRE(sum_ac.getDenominator() == 1);

        Rat sum_ad = rat_a + rat_d;
        REQUIRE(sum_ad.getNumerator() == 1);
        REQUIRE(sum_ad.getDenominator() == 4);

        Rat sum_cd = rat_c + rat_d;
        REQUIRE(sum_cd.getNumerator() == -3);
        REQUIRE(sum_cd.getDenominator() == 4);

        Rat sum_ae = rat_a + rat_e;
        REQUIRE(sum_ae.getNumerator() == 1);
        REQUIRE(sum_ae.getDenominator() == 2);

        Rat sum_ce = rat_c + rat_e;
        REQUIRE(sum_ce.getNumerator() == -1);
        REQUIRE(sum_ce.getDenominator() == 2);
    }

    SECTION("operator rat_a - rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(-1,4);
        Rat rat_e(0,0);

        Rat soustraction_ab = rat_a - rat_b;
        REQUIRE(soustraction_ab.getNumerator() == 1);
        REQUIRE(soustraction_ab.getDenominator() == 4);

        Rat soustraction_ba = rat_b - rat_a;
        REQUIRE(soustraction_ba.getNumerator() == -1);
        REQUIRE(soustraction_ba.getDenominator() == 4);

        Rat soustraction_aa = rat_a - rat_a;
        REQUIRE(soustraction_aa.getNumerator() == 0);
        REQUIRE(soustraction_aa.getDenominator() == 1);

        Rat soustraction_ac = rat_a - rat_c;
        REQUIRE(soustraction_ac.getNumerator() == 1);
        REQUIRE(soustraction_ac.getDenominator() == 1);

        Rat soustraction_ae = rat_a - rat_e;
        REQUIRE(soustraction_ae.getNumerator() == 1);
        REQUIRE(soustraction_ae.getDenominator() == 2);

        Rat soustraction_ea = rat_e - rat_a;
        REQUIRE(soustraction_ea.getNumerator() == -1);
        REQUIRE(soustraction_ea.getDenominator() == 2);
    }

    SECTION("operator rat_a * rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(-1,4);
        Rat rat_e(0,0);

        Rat multiplication_ab = rat_a * rat_b;
        REQUIRE(multiplication_ab.getNumerator() == 1);
        REQUIRE(multiplication_ab.getDenominator() == 8);

        Rat multiplication_bc = rat_b * rat_c;
        REQUIRE(multiplication_bc.getNumerator() == -1);
        REQUIRE(multiplication_bc.getDenominator() == 8);

        Rat multiplication_cd = rat_c * rat_d;
        REQUIRE(multiplication_cd.getNumerator() == 1);
        REQUIRE(multiplication_cd.getDenominator() == 8);

        Rat multiplication_ae = rat_a * rat_e;
        REQUIRE(multiplication_ae.getNumerator() == 0);
        REQUIRE(multiplication_ae.getDenominator() == 1);

        Rat multiplication_ce = rat_c * rat_e;
        REQUIRE(multiplication_ce.getNumerator() == 0);
        REQUIRE(multiplication_ce.getDenominator() == 1);
    }

    SECTION("operator rat_a / rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(-1,4);
        Rat rat_e(0,0);

        Rat divison_ab = rat_a / rat_b;
        REQUIRE(divison_ab.getNumerator() == 2);
        REQUIRE(divison_ab.getDenominator() == 1);

        Rat divison_ba = rat_b / rat_a;
        REQUIRE(divison_ba.getNumerator() == 1);
        REQUIRE(divison_ba.getDenominator() == 2);

        Rat divison_cb = rat_c / rat_b;
        REQUIRE(divison_cb.getNumerator() == -2);
        REQUIRE(divison_cb.getDenominator() == 1);

        Rat divison_cd = rat_c / rat_d;
        REQUIRE(divison_cd.getNumerator() == 2);
        REQUIRE(divison_cd.getDenominator() == 1);

        REQUIRE_THROWS(rat_a / rat_e);

        REQUIRE_THROWS(rat_c / rat_e);

        Rat divison_ea = rat_e / rat_a;
        REQUIRE(divison_ea.getNumerator() == 0);
        REQUIRE(divison_ea.getDenominator() == 1);

        Rat divison_ec = rat_e / rat_c;
        REQUIRE(divison_ec.getNumerator() == 0);
        REQUIRE(divison_ec.getDenominator() == 1);
    }

    SECTION("operator rat_a += rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(-1,4);
        Rat rat_e(0,0);

        Rat sum_1(1,2);
        sum_1 += rat_b;
        REQUIRE(sum_1.getNumerator() == 3);
        REQUIRE(sum_1.getDenominator() == 4);

        Rat sum_2(1,2);
        sum_2 += rat_e;
        REQUIRE(sum_2.getNumerator() == 1);
        REQUIRE(sum_2.getDenominator() == 2);

        Rat sum_3(1,2);
        sum_3 += rat_c;
        REQUIRE(sum_3.getNumerator() == 0);
        REQUIRE(sum_3.getDenominator() == 1);

        Rat sum_4(1,2);
        sum_4 += rat_d;
        REQUIRE(sum_4.getNumerator() == 1);
        REQUIRE(sum_4.getDenominator() == 4);

        Rat sum_5(0,0);
        sum_5 += rat_a;
        REQUIRE(sum_5.getNumerator() == 1);
        REQUIRE(sum_5.getDenominator() == 2);

        Rat sum_6(0,0);
        sum_6 += rat_c;
        REQUIRE(sum_6.getNumerator() == -1);
        REQUIRE(sum_6.getDenominator() == 2);

        Rat sum_7(0,0);
        sum_7 += rat_e;
        REQUIRE(sum_7.getNumerator() == 0);
        REQUIRE(sum_7.getDenominator() == 1);
    }

    SECTION("operator rat_a -= rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(-1,4);
        Rat rat_e(0,0);

        Rat soustraction_1(1,2);
        soustraction_1 -= rat_b;
        REQUIRE(soustraction_1.getNumerator() == 1);
        REQUIRE(soustraction_1.getDenominator() == 4);

        Rat soustraction_2(1,2);
        soustraction_2 -= rat_e;
        REQUIRE(soustraction_2.getNumerator() == 1);
        REQUIRE(soustraction_2.getDenominator() == 2);

        Rat soustraction_3(1,2);
        soustraction_3 -= rat_a;
        REQUIRE(soustraction_3.getNumerator() == 0);
        REQUIRE(soustraction_3.getDenominator() == 1);

        Rat soustraction_4(1,2);
        soustraction_4 -= rat_c;
        REQUIRE(soustraction_4.getNumerator() == 1);
        REQUIRE(soustraction_4.getDenominator() == 1);

        Rat soustraction_5(0,0);
        soustraction_5 -= rat_a;
        REQUIRE(soustraction_5.getNumerator() == -1);
        REQUIRE(soustraction_5.getDenominator() == 2);

        Rat soustraction_6(0,0);
        soustraction_6 -= rat_c;
        REQUIRE(soustraction_6.getNumerator() == 1);
        REQUIRE(soustraction_6.getDenominator() == 2);

        Rat soustraction_7(0,0);
        soustraction_7 -= rat_e;
        REQUIRE(soustraction_7.getNumerator() == 0);
        REQUIRE(soustraction_7.getDenominator() == 1);
    }

    SECTION("operator ++ (rat_a++)"){
        Rat rat_a(1,2);
        Rat sum_1 = rat_a++;
        REQUIRE(sum_1.getNumerator() == 1);
        REQUIRE(sum_1.getDenominator() == 2);
        REQUIRE(rat_a.getNumerator() == 3);
        REQUIRE(rat_a.getDenominator() == 2);

        Rat rat_b(0,0);
        Rat sum_2 = rat_b++;
        REQUIRE(sum_2.getNumerator() == 0);
        REQUIRE(sum_2.getDenominator() == 1);
        REQUIRE(rat_b.getNumerator() == 1);
        REQUIRE(rat_b.getDenominator() == 1);

        Rat rat_c(-1,2);
        Rat sum_3 = rat_c++;
        REQUIRE(sum_3.getNumerator() == -1);
        REQUIRE(sum_3.getDenominator() == 2);
        REQUIRE(rat_c.getNumerator() == 1);
        REQUIRE(rat_c.getDenominator() == 2);

        Rat rat_d(-3,1);
        Rat sum_4 = rat_d++;
        REQUIRE(sum_4.getNumerator() == -3);
        REQUIRE(sum_4.getDenominator() == 1);
        REQUIRE(rat_d.getNumerator() == -2);
        REQUIRE(rat_d.getDenominator() == 1);
    }

    SECTION("operator ++ (++rat_a)"){
        Rat rat_a(1,2);
        Rat sum_1 = ++rat_a;
        REQUIRE(sum_1.getNumerator() == 3);
        REQUIRE(sum_1.getDenominator() == 2);
        REQUIRE(rat_a.getNumerator() == 3);
        REQUIRE(rat_a.getDenominator() == 2);

        Rat rat_b(0,0);
        Rat sum_2 = ++rat_b;
        REQUIRE(sum_2.getNumerator() == 1);
        REQUIRE(sum_2.getDenominator() == 1);
        REQUIRE(rat_b.getNumerator() == 1);
        REQUIRE(rat_b.getDenominator() == 1);

        Rat rat_c(-1,2);
        Rat sum_3 = ++rat_c;
        REQUIRE(sum_3.getNumerator() == 1);
        REQUIRE(sum_3.getDenominator() == 2);
        REQUIRE(rat_c.getNumerator() == 1);
        REQUIRE(rat_c.getDenominator() == 2);

        Rat rat_d(-3,1);
        Rat sum_4 = ++rat_d;
        REQUIRE(sum_4.getNumerator() == -2);
        REQUIRE(sum_4.getDenominator() == 1);
        REQUIRE(rat_d.getNumerator() == -2);
        REQUIRE(rat_d.getDenominator() == 1);
    }

    SECTION("operator -- (--rat_a)"){
        Rat rat_a(1,2);
        Rat soustraction_1 = --rat_a;
        REQUIRE(soustraction_1.getNumerator() == -1);
        REQUIRE(soustraction_1.getDenominator() == 2);
        REQUIRE(rat_a.getNumerator() == -1);
        REQUIRE(rat_a.getDenominator() == 2);

        Rat rat_b(0,0);
        Rat soustraction_2 = --rat_b;
        REQUIRE(soustraction_2.getNumerator() == -1);
        REQUIRE(soustraction_2.getDenominator() == 1);
        REQUIRE(rat_b.getNumerator() == -1);
        REQUIRE(rat_b.getDenominator() == 1);

        Rat rat_c(-1,2);
        Rat soustraction_3 = --rat_c;
        REQUIRE(soustraction_3.getNumerator() == -3);
        REQUIRE(soustraction_3.getDenominator() == 2);
        REQUIRE(rat_c.getNumerator() == -3);
        REQUIRE(rat_c.getDenominator() == 2);

        Rat rat_d(2,1);
        Rat soustraction_4 = --rat_d;
        REQUIRE(soustraction_4.getNumerator() == 1);
        REQUIRE(soustraction_4.getDenominator() == 1);
        REQUIRE(rat_d.getNumerator() == 1);
        REQUIRE(rat_d.getDenominator() == 1);
    }

    SECTION("operator -- (rat_a--)"){
        Rat rat_a(1,2);
        Rat soustraction_1 = rat_a--;
        REQUIRE(soustraction_1.getNumerator() == 1);
        REQUIRE(soustraction_1.getDenominator() == 2);
        REQUIRE(rat_a.getNumerator() == -1);
        REQUIRE(rat_a.getDenominator() == 2);

        Rat rat_b(0,0);
        Rat soustraction_2 = rat_b--;
        REQUIRE(soustraction_2.getNumerator() == 0);
        REQUIRE(soustraction_2.getDenominator() == 1);
        REQUIRE(rat_b.getNumerator() == -1);
        REQUIRE(rat_b.getDenominator() == 1);

        Rat rat_c(-1,2);
        Rat soustraction_3 = rat_c--;
        REQUIRE(soustraction_3.getNumerator() == -1);
        REQUIRE(soustraction_3.getDenominator() == 2);
        REQUIRE(rat_c.getNumerator() == -3);
        REQUIRE(rat_c.getDenominator() == 2);

        Rat rat_d(2,1);
        Rat soustraction_4 = rat_d--;
        REQUIRE(soustraction_4.getNumerator() == 2);
        REQUIRE(soustraction_4.getDenominator() == 1);
        REQUIRE(rat_d.getNumerator() == 1);
        REQUIRE(rat_d.getDenominator() == 1);
    }

    SECTION("operator == (rat_a == rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(2,4);
        Rat rat_e(0,0);

        REQUIRE(rat_a == rat_a);
        REQUIRE(rat_a == rat_d);
        REQUIRE(!(rat_a == rat_b));
        REQUIRE(rat_e == rat_e);
        REQUIRE(!(rat_e == rat_a));
        REQUIRE(!(rat_c == rat_a));
        REQUIRE(rat_c == rat_c);
    }

    SECTION("operator != (rat_a != rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(2,4);
        Rat rat_e(0,0);

        REQUIRE(!(rat_a != rat_a));
        REQUIRE(!(rat_a != rat_d));
        REQUIRE(rat_a != rat_b);
        REQUIRE(!(rat_e != rat_e));
        REQUIRE(rat_e != rat_a);
        REQUIRE(rat_c != rat_a);
        REQUIRE(!(rat_c != rat_c));
    }

    SECTION("operator < (rat_a < rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(2,4);
        Rat rat_e(0,0);

        REQUIRE(!(rat_a < rat_a));
        REQUIRE(!(rat_a < rat_b));
        REQUIRE(rat_b < rat_a);
        REQUIRE(rat_e < rat_a);
        REQUIRE(!(rat_a < rat_e));
        REQUIRE(rat_c < rat_b);
        REQUIRE(rat_c < rat_e);
    }

    SECTION("operator > (rat_a > rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(2,4);
        Rat rat_e(0,0);

        REQUIRE(!(rat_a > rat_a));
        REQUIRE(rat_a > rat_b);
        REQUIRE(!(rat_b > rat_a));
        REQUIRE(!(rat_e > rat_a));
        REQUIRE(rat_a > rat_e);
        REQUIRE(!(rat_c > rat_b));
        REQUIRE(!(rat_c > rat_e));
    }

    SECTION("operator >= (rat_a > rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(2,4);
        Rat rat_e(0,0);

        REQUIRE(rat_a >= rat_a);
        REQUIRE(rat_a >= rat_b);
        REQUIRE(!(rat_b >= rat_a));
        REQUIRE(!(rat_e >= rat_a));
        REQUIRE(rat_a >= rat_e);
        REQUIRE(!(rat_c >= rat_b));
        REQUIRE(!(rat_c >= rat_e));
    }

    SECTION("operator <= (rat_a <= rat_b"){
        Rat rat_a(1,2);
        Rat rat_b(1,4);
        Rat rat_c(-1,2);
        Rat rat_d(2,4);
        Rat rat_e(0,0);

        REQUIRE(rat_a <= rat_a);
        REQUIRE(!(rat_a <= rat_b));
        REQUIRE(rat_b <= rat_a);
        REQUIRE(rat_e <= rat_a);
        REQUIRE(!(rat_a <= rat_e));
        REQUIRE(rat_c <= rat_b);
        REQUIRE(rat_c <= rat_e);
    }

    SECTION("operator << (<< rat_a"){
        //std::ostream& operator<< (std::ostream& out, const Rat r);

        Rat rat_a(1,2);
        std::stringstream ss;
        ss << rat_a;
        std::string my_string = ss.str();
        REQUIRE(my_string == "1 / 2");


        Rat rat_b(-1,2);
        std::stringstream ss2;
        ss2 << rat_b;
        std::string my_string2 = ss2.str();
        REQUIRE(my_string2 == "-1 / 2");

        Rat rat_c(0,0);
        std::stringstream ss3;
        ss3 << rat_c;
        std::string my_string3 = ss3.str();
        REQUIRE(my_string3 == "0 / 0");
    }

}