//
// Created by beat on 30.01.19.
//

#include "../../Catch2/catch.hpp"

#include "../../core/basic/matrix2d.h"
#include "../../core/rational/rat.h"
#include <iostream>
#include <sstream>

// std::cout UnitTest source website
// https://stackoverflow.com/questions/4191089/how-to-unit-test-function-writing-to-stdout-stdcout
// https://stackoverflow.com/questions/4810516/c-redirecting-stdout
// https://stackoverflow.com/questions/5288036/how-to-clear-ostringstream

        template <template<class> class MatrixType, class ValueType>
        bool checkMatrixContent(MatrixType<ValueType> &matrix, ValueType v){
            for(int c = 0; c < matrix.getCols(); c++){
                for(int l = 0; l < matrix.getLines(); l++){
            REQUIRE(matrix.get(l, c) == v);
        }
    }
    return true;
}

template <template<class> class MatrixType, class ValueType>
void testEquality(MatrixType<ValueType> &matrix){

    SECTION("Equality test"){
        REQUIRE(matrix == matrix);
        REQUIRE(!(matrix != matrix));

        matrix.fill(3);
        MatrixType<ValueType> matrix2(matrix.getLines(), matrix.getCols());
        matrix2.fill(3);


        REQUIRE(matrix == matrix2);
        REQUIRE(!(matrix != matrix2));

        matrix2.fill(4);

        REQUIRE(matrix != matrix2);
        REQUIRE(!(matrix == matrix2));
    }

}

template <template<class> class MatrixType, class ValueType>
void testMatrixInit(MatrixType<ValueType> &matrix){
    SECTION("Zeroed"){
        matrix.fill(0);

        checkMatrixContent<Matrix2D, ValueType>(matrix, 0);
        testEquality(matrix);
    }

    SECTION("Random"){
        matrix.random();

        ValueType last = 0;
        int different = 0;
        for(int c = 0; c < matrix.getCols(); c++) {
            for (int l = 0; l < matrix.getLines(); l++) {
                if(matrix.get(l, c) != last){
                    different++;
                }
                last = matrix.get(l, c);
            }
        }
        REQUIRE(different >= ((matrix.getCols() * matrix.getLines()) / 4));

        testEquality(matrix);
    }

    SECTION("Store and load"){
        for(int c = 0; c < matrix.getCols(); c++) {
            for (int l = 0; l < matrix.getLines(); l++) {
                matrix.set(l, c, l * c);
            }
        }

        for(int c = 0; c < matrix.getCols(); c++){
            for(int l = 0; l < matrix.getLines(); l++){
                REQUIRE(matrix.get(l, c) == l * c);
            }
        }

        testEquality(matrix);
    }
}

TEMPLATE_TEST_CASE( "1: Test matrix creation", "MatrixTests", int, double, Rat) {

    SECTION("Square") {
        for (int i = 1; i < 100; i++) {
            Matrix2D<TestType> matrix(i, i);

            REQUIRE(matrix.getCols() == i);
            REQUIRE(matrix.getLines() == i);

            testMatrixInit(matrix);
        }
    }

    SECTION("Rectangular"){
        for(int i = 1; i < 100; i++){

            for(int loop = 1; loop < 100; loop++){
                if(i != loop){
                    Matrix2D<TestType> matrix(i, loop);

                    REQUIRE(matrix.getLines() == i);
                    REQUIRE(matrix.getCols() == loop);

                    testMatrixInit(matrix);
                }
            }
        }
    }

}

TEMPLATE_TEST_CASE( "1: Test matrix calculations", "MatrixTests", int, double, Rat) {
    Matrix2D<TestType> a(3,3);
    Matrix2D<TestType> b(3,3);
    Matrix2D<TestType> e(2,3);

    a.fill(1);
    b.fill(2);
    e.fill(1);


    SECTION("Plus"){
        Matrix2D<TestType> c = a + b;
        Matrix2D<TestType> d(3,3);
        d.fill(3);

        SECTION("Not same matrix size"){
            //REQUIRE_THROWS(a + e);
        }

        SECTION("Size check"){
            REQUIRE(c.getCols() == 3);
            REQUIRE(c.getLines() == 3);
        }

        SECTION("Unchanged source"){
            REQUIRE(checkMatrixContent<Matrix2D, TestType>(a, 1));
            REQUIRE(checkMatrixContent<Matrix2D, TestType>(b, 2));
        }

        SECTION("Result"){
            REQUIRE(c == d);
            REQUIRE(checkMatrixContent<Matrix2D, TestType>(c, 3));
        }

        SECTION("Randomness"){
            a.random();
            b.random();

            c = a + b;

            for(int col = 0; col < c.getCols(); col++) {
                for (int line = 0; line < c.getLines(); line++) {
                    REQUIRE(c.get(line, col) == (a.get(line, col) + b.get(line, col)));
                }
            }
        }
    }

    SECTION("Minus"){
        Matrix2D<TestType> c = a - b;
        Matrix2D<TestType> d(3,3);
        d.fill(-1);


        SECTION("Not same matrix size"){
            //REQUIRE_THROWS(a - e);
        }

        SECTION("Result"){
            REQUIRE(c.getCols() == 3);
            REQUIRE(c.getLines() == 3);
            REQUIRE(c == d);

            REQUIRE(checkMatrixContent<Matrix2D, TestType>(c, -1));
        }

        SECTION("Randomness"){
            a.random();
            b.random();

            c = a - b;

            for(int col = 0; col < c.getCols(); col++) {
                for (int line = 0; line < c.getLines(); line++) {
                    REQUIRE(c.get(line, col) == (a.get(line, col) - b.get(line, col)));
                }
            }
        }
    }
}

TEMPLATE_TEST_CASE( "1: Matrix multiplication", "MatrixTests", int, double, Rat) {
    Matrix2D<TestType> a(2,2);
    Matrix2D<TestType> b(2,2);

    SECTION("SQUARE"){
        a.set(0, 0, 1);
        a.set(0, 1, 2);
        a.set(1, 0, 3);
        a.set(1, 1, 4);

        b.set(0, 0, 2);
        b.set(0, 1, 0);
        b.set(1, 0, 1);
        b.set(1, 1, 2);

        Matrix2D<TestType> c = a * b;

        SECTION("Size check"){
            REQUIRE(c.getCols() == 2);
            REQUIRE(c.getLines() == 2);
        }

        SECTION("Results"){
            REQUIRE(c.get(0,0) == 4);
            REQUIRE(c.get(0,1) == 4);
            REQUIRE(c.get(1,0) == 10);
            REQUIRE(c.get(1,1) == 8);
        }
    }


    SECTION("RECTANGULAR"){
        Matrix2D<TestType> x = Matrix2D<TestType>(3, 2);
        Matrix2D<TestType> y = Matrix2D<TestType>(2, 3);
        x.fill(3);
        y.fill(2);
        Matrix2D<TestType> c = x * y;

        SECTION("Size check"){
            REQUIRE(c.getCols() == 3);
            REQUIRE(c.getLines() == 3);
        }

        SECTION("Results"){
            REQUIRE(checkMatrixContent<Matrix2D, TestType>(c, 12));
        }
    }

}


TEMPLATE_TEST_CASE( "1: Advanced tests", "MatrixTests", int, double, Rat) {
    Matrix2D<TestType> a(2,2);
    Matrix2D<TestType> b(2,2);


    a.fill(1);
    b.fill(2);

    Matrix2D<TestType> c = (a * b) - (a + b * b) + a * a;

    REQUIRE(checkMatrixContent<Matrix2D, TestType>(c, -3));
}

TEMPLATE_TEST_CASE( "1: Operators", "MatrixTests", int, double, Rat) {

    Matrix2D<TestType> a(2,2);
    Matrix2D<TestType> b(2,2);
    Matrix2D<TestType> c(2,2);
    Matrix2D<TestType> d(3,2);

    a.fill(3);
    b.fill(3);
    c.fill(6);
    d.fill(3);

    SECTION("operator =="){
        REQUIRE(a == b);
        REQUIRE(!(a == c));
        REQUIRE(!(a == d));
    }

    SECTION("operator !="){
        REQUIRE(!(a != b));
        REQUIRE(a != c);
        REQUIRE(a != d);
    }
}


TEMPLATE_TEST_CASE( "1: Tests Bastien for others methods", "MatrixTests", int, double, Rat) {

    SECTION("methods get") {
        Matrix2D<TestType> a(2, 2);
        a.fill(1);
        REQUIRE(a.get(1, 1) == 1);
    }

    SECTION("methods set") {
        Matrix2D<TestType> a(2, 2);
        a.fill(1);
        a.set(0, 1, 9);
        REQUIRE(a.get(0, 1) == 9);
    }

    SECTION("copy constructor"){
        Matrix2D<TestType> a(3,2);
        a.fill(4);

        Matrix2D<TestType> b = a;

        REQUIRE(b.get(2,1) == 4);
    }

    Matrix2D<TestType> a(2,2);
    a.fill(1);

    Matrix2D<TestType> b(4,2);
    b.fill(2);

    SECTION("Random matrix"){
        Matrix2D<TestType> matrix(4,4);
        matrix.random();

        TestType last = 0;
        int different = 0;
        for(int c = 0; c < matrix.getCols(); c++) {
            for (int l = 0; l < matrix.getLines(); l++) {
                if(matrix.get(l, c) != last){
                    different++;
                }
                last = matrix.get(l, c);
            }
        }
        REQUIRE(different >= ((matrix.getCols() * matrix.getLines()) / 4));

        testEquality(matrix);
    }
}

TEST_CASE("1: methods display() and display(int)", "MatrixTests"){
    SECTION("(int)"){
        Matrix2D<int> a(2,2);
        a.fill(1);
        std::string a_expected = "| 1 1 |\n"
                                 "| 1 1 |\n";

        Matrix2D<int> b(4,2);
        b.fill(2);
        std::string b_expected = "| 2 2 |\n"
                                 "| 2 2 |\n";
        // Redirect cout.
        std::streambuf* oldCoutStreamBuf = std::cout.rdbuf();
        std::ostringstream strCout;
        std::cout.rdbuf( strCout.rdbuf() );
        // This goes to the string stream.
        a.display();
        REQUIRE(strCout.str() == a_expected);
        strCout.str("");
        strCout.clear();
        b.display(2);
        REQUIRE(strCout.str() == b_expected);
        // Restore old cout.
        std::cout.rdbuf( oldCoutStreamBuf );
    }

    SECTION("(double)"){
        Matrix2D<double> a(2,2);
        a.fill(1);
        std::string a_expected = "| 1 1 |\n"
                                 "| 1 1 |\n";

        Matrix2D<double> b(4,2);
        b.fill(2);
        std::string b_expected = "| 2 2 |\n"
                                 "| 2 2 |\n";
        // Redirect cout.
        std::streambuf* oldCoutStreamBuf = std::cout.rdbuf();
        std::ostringstream strCout;
        std::cout.rdbuf( strCout.rdbuf() );
        // This goes to the string stream.
        a.display();
        REQUIRE(strCout.str() == a_expected);
        strCout.str("");
        strCout.clear();
        b.display(2);
        REQUIRE(strCout.str() == b_expected);
        // Restore old cout.
        std::cout.rdbuf( oldCoutStreamBuf );
    }

    SECTION("(Rat)"){
        Matrix2D<Rat> a(2,2);
        a.fill(1);
        std::string a_expected = "| 1 / 1 1 / 1 |\n"
                                 "| 1 / 1 1 / 1 |\n";

        Matrix2D<Rat> b(4,2);
        b.fill(2);
        std::string b_expected = "| 2 / 1 2 / 1 |\n"
                                 "| 2 / 1 2 / 1 |\n";
        // Redirect cout.
        std::streambuf* oldCoutStreamBuf = std::cout.rdbuf();
        std::ostringstream strCout;
        std::cout.rdbuf( strCout.rdbuf() );
        // This goes to the string stream.
        a.display();
        REQUIRE(strCout.str() == a_expected);
        strCout.str("");
        strCout.clear();
        b.display(2);
        REQUIRE(strCout.str() == b_expected);
        // Restore old cout.
        std::cout.rdbuf( oldCoutStreamBuf );
    }
}
