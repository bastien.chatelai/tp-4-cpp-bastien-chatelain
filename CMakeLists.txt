cmake_minimum_required(VERSION 3.10)
project(cpp_matrix)

set(CMAKE_CXX_STANDARD 17)

file(GLOB_RECURSE CORE_SOURCES core/*.h core/*.cpp core/*.cc)
file(GLOB_RECURSE TEST_SOURCES tests/*.h tests/*.cpp tests/*.cc)

FIND_PACKAGE(OpenMP REQUIRED)
if(OPENMP_FOUND)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
else()
    message(WARNING "OpenMP could not be enabled")
endif()

add_library(matrix_lib ${CORE_SOURCES})

add_executable(unittests ${TEST_SOURCES})
target_link_libraries(unittests matrix_lib)


add_executable(main main.cpp)
target_link_libraries(main matrix_lib)

add_executable(main_2 main_2.cpp)
target_link_libraries(main_2 matrix_lib)
#target_link_libraries(main_2 profiler)

set(CMAKE_BUILD_TYPE Release)
#set(CMAKE_BUILD_TYPE Debug)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Werror -O3")

#if(NOT MSVC)
#    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Werror")
#endif(NOT MSVC)