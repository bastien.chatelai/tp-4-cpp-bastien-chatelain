#include "core/basic/matrix2d.h"

#include <iostream>
#include <chrono>

template<class ValueType>
void benchmarkMultiplication(int size, int repetitions){
    Matrix2D<ValueType> a(size, size);
    Matrix2D<ValueType> b(size, size);

    a.random();
    b.random();
    auto start = std::chrono::system_clock::now();


    for(int i = 0;i < repetitions ; i++){
        Matrix2D<ValueType> c = a * b;
    }

    auto passedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);

    std::cout << std::fixed << "Finished multiplication (" << size << ") in avg. " << (passedTime.count() / repetitions)<< "ms, total " << passedTime.count() << "ms" << std::endl;
}

template<class ValueType>
void benchmarkAddition(int size, int repetitions){
    Matrix2D<ValueType> a(size, size);
    Matrix2D<ValueType> b(size, size);

    a.random();
    b.random();

    auto start = std::chrono::system_clock::now();

    for(int i = 0;i < repetitions ; i++){
        Matrix2D<ValueType> c = a + b;
    }

    auto passedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);

    std::cout << std::fixed << "Finished addition (" << size << ") in avg. " << (passedTime.count() / repetitions)<< "ms, total " << passedTime.count() << "ms" << std::endl;
}

template<class ValueType>
void benchmarkSubstraction(int size, int repetitions){
    Matrix2D<ValueType> a(size, size);
    Matrix2D<ValueType> b(size, size);

    a.random();
    b.random();

    auto start = std::chrono::system_clock::now();

    for(int i = 0;i < repetitions ; i++){
        Matrix2D<ValueType> c = a - b;
    }

    auto passedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);

    std::cout << std::fixed << "Finished substraction (" << size << ") in avg. " << (passedTime.count() / repetitions)<< "ms, total " << passedTime.count() << "ms" << std::endl;
}


int main() {
    // TODO: test avec int, double, Rat

    std::cout << "Test with int" << std::endl;
    benchmarkAddition<int>(1000, 1000);
    benchmarkAddition<int>(3000, 50);

    benchmarkSubstraction<int>(1000, 1000);
    benchmarkSubstraction<int>(3000, 50);

    benchmarkMultiplication<int>(100, 100);
    benchmarkMultiplication<int>(500, 3);
    benchmarkMultiplication<int>(1000, 3);
    benchmarkMultiplication<int>(2000, 1);

    std::cout << "Test with double" << std::endl;
    benchmarkAddition<double>(1000, 1000);
    benchmarkAddition<double>(3000, 50);

    benchmarkSubstraction<double>(1000, 1000);
    benchmarkSubstraction<double>(3000, 50);

    benchmarkMultiplication<double>(100, 100);
    benchmarkMultiplication<double>(500, 3);
    benchmarkMultiplication<double>(1000, 3);
    benchmarkMultiplication<double>(2000, 1);

    std::cout << "Test with Rat" << std::endl;
    benchmarkAddition<Rat>(1000, 1000);
    benchmarkAddition<Rat>(3000, 50);

    benchmarkSubstraction<Rat>(1000, 1000);
    benchmarkSubstraction<Rat>(3000, 50);

    benchmarkMultiplication<Rat>(10, 100);
    benchmarkMultiplication<Rat>(50, 3);
    benchmarkMultiplication<Rat>(100, 3);
    benchmarkMultiplication<Rat>(200, 1);

    return 0;
}