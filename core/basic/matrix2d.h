//
// Created by beat on 06.12.16.
//

#ifndef TEST_MATRIX2D_H
#define TEST_MATRIX2D_H

#include <vector>
#include <iostream>
#include <random>
#include "../../core/rational/rat.h"

template <class ValueType>
class Matrix2D                //mxn 2D Matrix with values of type "ValueType"
{
public:
    Matrix2D();                    // Default constructor, create an empty matrix 0x0
    Matrix2D(int line, int col);  // Constructor, create a 'line'x'col' empty matrix
    Matrix2D(const Matrix2D<ValueType>& m);  // Copy constructor
    ~Matrix2D();          // Destructor

    int getLines() const;  // get number of lines
    int getCols() const;   // get number of columns

    void random();                // Fill matrix random values
    void fill(ValueType v);       // Fill matrix with v
    void set(int line, int col, ValueType v);       // set the value at line,col
    ValueType get(int line, int col) const;  // get the value at line,col

    Matrix2D& operator=(const Matrix2D& m); // assignment operator for Matrix2D

    bool operator==(const Matrix2D& m) const; // equality test operator for Matrix2D
    bool operator!=(const Matrix2D& m) const; // equality test operator for Matrix2D
    Matrix2D operator+(const Matrix2D& m) const;// addition operator for Matrix2D
    Matrix2D operator-(const Matrix2D& m) const;// substraction operator for Matrix2D
    Matrix2D operator*(const Matrix2D& m) const; // multiply operator for Matrix2D

    void display() const;       // Display the matrix content
    void display(int n) const;  // Display the first N columns/lines partially the matrix content

private:
    int _number_line;
    int _number_col;

    std::vector<std::vector<ValueType>> _matrix;
};

/*
 * Default Constructor (col = 0 and line = 0)
 * @return void
 */
template<class ValueType>
inline Matrix2D<ValueType>::Matrix2D(): _number_line(0), _number_col(0){
    _matrix.resize(_number_line, std::vector<ValueType>(_number_col));
}

/*
 * Constructor
 * @param line(int)
 * @param col(int)
 * @return void
 */
template<class ValueType>
inline Matrix2D<ValueType>::Matrix2D(int line, int col):_number_line(line), _number_col(col){
    _matrix.resize(_number_line, std::vector<ValueType>(_number_col));
}

/*
 * Copy Constructor
 * @param m (Matrix2D)
 * @return void
 */
template<class ValueType>
inline Matrix2D<ValueType>::Matrix2D(const Matrix2D<ValueType>& m){
    _number_col = m._number_col;
    _number_line = m._number_line;
    _matrix.resize(_number_line, std::vector<ValueType>(_number_col));

    #pragma omp parallel for simd //default(none) shared(m)
    for (int line = 0; line < _number_line; line++){
        for (int col = 0; col < _number_col; col++){
            _matrix[line][col] = m._matrix[line][col];
        }
    }
}

template<class ValueType>
inline Matrix2D<ValueType>::~Matrix2D(){
}

/*
 * Get number of line of the matrix
 * @return numberOfLine(int)
 */
template<class ValueType>
inline int Matrix2D<ValueType>::getLines() const {
    return _number_line;
}

/*
 * Get number of column of the matrix
 * @return numberOfColumn(int)
 */
template<class ValueType>
inline int Matrix2D<ValueType>::getCols() const {
    return _number_col;
}

/*
 * Fill matrix with random value (Generic type)
 * @return void
 */
template<class ValueType>
void Matrix2D<ValueType>::random() {
    std::random_device rd;
    std::default_random_engine eng(rd());
    std::uniform_int_distribution<int> random_number(0, 50);
    #pragma omp parallel for default(none) shared(_matrix, random_number, eng)
    for (int row = 0; row < _number_line; row++){
        for (int col = 0; col < _number_col; col++){
            _matrix[row][col] = random_number(eng);
        }
    }

}


/*
 * Fill matrix with random value (Rat)
 * @return void
 */
template<>
void Matrix2D<Rat>::random() {
    std::random_device rd;
    std::default_random_engine eng(rd());
    std::uniform_int_distribution<int> random_numerator(0, 20);
    std::uniform_int_distribution<int> random_denominator(1, 25);
    #pragma omp parallel for default(none) shared(_matrix, random_numerator,random_denominator,eng)
    for (int row = 0; row < _number_line; row++){
        for (int col = 0; col < _number_col; col++){
            _matrix[row][col] = Rat(random_numerator(eng), random_denominator(eng));
        }
    }
}

/*
 * Fill matrix with a value
 * @param v(ValueType)
 * @return void
 */
template<class ValueType>
inline void Matrix2D<ValueType>::fill(ValueType v) {
    #pragma omp parallel for simd //default(none) shared(_matrix, v)
    for (int line = 0; line < _number_line; line++){
        for (int col = 0; col < _number_col; col++){
            _matrix[line][col] = v;
        }
    }
}

/*
 * Set a value in matrix
 * @param line(int)
 * @param col(int)
 * @param v(ValueType)
 * @return void
 */
template<class ValueType>
inline void Matrix2D<ValueType>::set(int line, int col, ValueType v){
    _matrix[line][col] = v;
}

/*
 * Get value in the matrix
 * @param line(int)
 * @param col(int)
 * @return valueInTheMatrix(ValueType)
 */
template<class ValueType>
inline ValueType Matrix2D<ValueType>::get(int line, int col) const{
    return _matrix[line][col];
}

/*
 * Operator = (matrix_a = matrix_b)
 * @param m(const Matrix2D&)
 * @return matrix(Matrix2D&)
 */
template<class ValueType>
inline Matrix2D<ValueType>& Matrix2D<ValueType>::operator=(const Matrix2D& m){
    _matrix.resize(m._number_line, std::vector<ValueType>(m._number_col));
    _number_col = m._number_col;
    _number_line = m._number_line;
    #pragma omp parallel for //default(none) shared(m)
    for (int row = 0; row < _number_line; row++){
        #pragma omp simd
        for (int col = 0; col < _number_col; col++){
            _matrix[row][col] = m._matrix[row][col];
        }
    }
    return *this;
}

/*
 * Operator == (matrix_a == matrix_b)
 * @param m(Matrix2D)
 * @return isEqual(bool)
 */
template<class ValueType>
bool Matrix2D<ValueType>::operator==(const Matrix2D& m) const{
    if (_number_line != m.getLines() || _number_col != m.getCols()) return false;
    for (int line = 0; line < _number_line; line++){
        for (int col = 0; col < _number_col; col++){
            if (this->get(line, col) != m.get(line, col)) return false;
        }
    }
    return true;
}

/*
 * Operator != (matrix_a != matrix_b)
 * @param m(Matrix2D)
 * @return isNotEqual(bool)
 */
template<class ValueType>
bool Matrix2D<ValueType>::operator!=(const Matrix2D& m) const{
    return !(*(this) == m);
}

/*
 * Operator + (matrix_a + matrix_b)
 * @param m(Matrix2D)
 * @return computedMatrix(Matrix2D)
 */
template<class ValueType>
inline Matrix2D<ValueType> Matrix2D<ValueType>::operator+(const Matrix2D& m) const{
    //if ((_number_line != m.getLines()) || (_number_col != m.getCols())) throw std::out_of_range("Error");
    Matrix2D<ValueType> newMatrix(_number_line, _number_col);
    #pragma omp parallel for simd //default(none) shared(newMatrix,m)
    for (int line = 0; line < _number_line; line++){
        for (int col = 0; col < _number_col; col++){
            newMatrix._matrix[line][col] = _matrix[line][col] + m._matrix[line][col];
        }
    }
    return newMatrix;
}

/*
 * Operator - (matrix_a - matrix_b)
 * @param m(Matrix2D)
 * @return computedMatrix(Matrix2D)
 */
template<class ValueType>
inline Matrix2D<ValueType> Matrix2D<ValueType>::operator-(const Matrix2D& m) const{
    //if ((_number_line != m.getLines()) || (_number_col != m.getCols())) throw std::out_of_range("Error");
    Matrix2D<ValueType> newMatrix(_number_line, _number_col);
    #pragma omp parallel for simd //default(none) shared(newMatrix, m)
    for (int line = 0; line < _number_line; line++){
        for (int col = 0; col < _number_col; col++){
            newMatrix._matrix[line][col] = _matrix[line][col] - m._matrix[line][col];
        }
    }
    return newMatrix;
}

/*
 * Operator * => matrix multiplication (matrix_a * matrix_b)
 * @param m(const Matrix2D&)
 * @return computedMatrix(Matrix2D)
 */
template<class ValueType>
inline Matrix2D<ValueType> Matrix2D<ValueType>::operator*(const Matrix2D& m) const{
    //if (_number_col != m.getLines()) throw std::domain_error("Invalid matrix size");

    Matrix2D<ValueType> newMatrix(_number_line, m._number_col);
    newMatrix.fill(0);
    #pragma omp parallel for //default(none) shared(newMatrix,m)
    for (int i = 0; i < _number_line; i++){
        #pragma omp parallel for //default(none) shared(newMatrix,m,i)
        for (int k = 0; k < m._number_line; k++){
                ValueType temp = _matrix[i][k];
                #pragma omp simd
                for (int j = 0; j < m._number_col; j++){
                    newMatrix._matrix[i][j] += temp * m._matrix[k][j];
                }
            }
        }
    return newMatrix;
}

/*
template<class ValueType>
Matrix2D<ValueType> Matrix2D<ValueType>::operator*(const Matrix2D& m) const{
    if (_number_col != m.getLines()) throw std::domain_error("Invalid matrix size");

    Matrix2D<ValueType> newMatrix(_number_line, m.getCols());
    newMatrix.fill(0);
    #pragma omp parallel for default(none) shared(newMatrix,m)
    for (int rowNewMatrix = 0; rowNewMatrix < newMatrix.getLines(); rowNewMatrix++){
        #pragma omp parallel for default(none) shared(newMatrix,m,rowNewMatrix)
        for (int colNewMatrix = 0; colNewMatrix < m.getCols(); colNewMatrix++){
            ValueType sum = 0;
            for (int colA = 0; colA < _number_col; colA++){
                sum += this->get(rowNewMatrix, colA) * m.get(colA, colNewMatrix);
            }
            newMatrix.set(rowNewMatrix, colNewMatrix, sum);
        }
    }
    return newMatrix;
}
 */

template<class ValueType>
void Matrix2D<ValueType>::display() const{
    for (int row = 0; row < _number_line; row++){
        std::cout << "|";
        for (int col = 0; col < _number_col; col++){
            std::cout << " " << _matrix[row][col];
        }
        std::cout << " |" << std::endl;
    }
}

template<class ValueType>
void Matrix2D<ValueType>::display(int n) const{
    for (int row = 0; row < n; row++){
        std::cout << "|";
        for (int col = 0; col < n; col++){
            std::cout << " " << _matrix[row][col];
        }
        std::cout << " |" << std::endl;
    }
}

#endif //TEST_MATRIX2D_H
