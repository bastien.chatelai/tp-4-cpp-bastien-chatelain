#include "rat.h"
#include <numeric>
#include <math.h>

/*
 * Default Constructor (zero : num = 0, den = 1)
 * @return void
 */
Rat::Rat():numerator(0), denominator(1){}

/*
 * Constructor (zero : num = 0, den = 1)
 * @param num(int)
 * @param : den(int)
 * @return void
 */
Rat::Rat(int num, int den) : numerator(num), denominator(den){
    if (denominator == 0){
        if (numerator == 0){
            denominator = 1;
        } else {
            throw std::domain_error{"Zero Denominator"};
        }
    }

    this->adjustSign();
    this->simplifyRational();
}

/*
 * Constructor
 * @param value(double)
 * @param accuracy(int)
 * @return void
 */
Rat::Rat(const double value, const int accuracy):denominator(pow(10, accuracy)){
    if (accuracy < 0){
        throw std::domain_error("Negative accuracy not permitted");
    }
    numerator = value*denominator;

    this->adjustSign();
    this->simplifyRational();

}

/*
 * Get numerator of Rat
 * @return numerator(int)
 */
int Rat::getNumerator() const {return numerator;};

/*
 * Get denumerator of Rat
 * @return denumerator(int)
 */
int Rat::getDenominator() const {return denominator;};

/*
 * Operator + (example : +my_rat)
 * @return same Rat (Rat)
 */
Rat Rat::operator+() const {
    return *(this);
}

/*
 * Operator - (example : -my_rat)
 * @return opposite Rat (Rat)
 */
Rat Rat::operator-() const {
    return Rat(-numerator, denominator);
}

/*
 * Operator + (example : rat_a + rat_b)
 * @param operand (Rat)
 * @return Rat + operand (Rat)
 */
Rat Rat::operator+(const Rat& operand) const {
    int newDen = denominator * operand.getDenominator();
    int newNum1 = operand.getDenominator() * numerator;
    int newNum2 = denominator * operand.getNumerator();

    return Rat(newNum1+newNum2, newDen);

}

/*
 * Operator - (example : rat_a - rat_b)
 * @param operand (Rat)
 * @return Rat - operand (Rat)
 */
Rat Rat::operator-(const Rat& operand) const {
    int newDen = denominator * operand.getDenominator();
    int newNum1 = operand.getDenominator() * numerator;
    int newNum2 = denominator * operand.getNumerator();

    return Rat(newNum1-newNum2, newDen);
}

/*
 * Operator * (example : rat_a * rat_b)
 * @param operand (Rat)
 * @return Rat * operand (Rat)
 */
Rat Rat::operator*(const Rat& operand) const {
    return Rat(numerator*operand.getNumerator(), denominator*operand.getDenominator());
}

/*
 * Operator / (example : rat_a / rat_b)
 * @param operand (Rat)
 * @return Rat / operand (Rat)
 */
Rat Rat::operator/(const Rat& operand) const {
    return Rat(numerator*operand.getDenominator(), denominator*operand.getNumerator());
}

/*
 * Operator += (example : my_rat += rat_b)
 * @param operand (Rat)
 * @return Rat + operand (Rat)
 */
Rat Rat::operator+=(const Rat& operand) {
    Rat sum = *(this)+operand;
    numerator = sum.getNumerator();
    denominator = sum.getDenominator();
    return sum;
}

/*
 * Operator -= (example : my_rat -= rat_b)
 * @param operand (Rat)
 * @return Rat - operand (Rat)
 */
Rat Rat::operator-=(const Rat& operand) {
    Rat soustraction = *(this)-operand;
    numerator = soustraction.getNumerator();
    denominator = soustraction.getDenominator();
    return soustraction;
}

/*
 * Operator ++ (example : ++my_rat)
 * @return Rat + 1 (Rat)
 */
Rat Rat::operator++() {
    Rat sum = *(this)+Rat(1,1);
    numerator = sum.getNumerator();
    denominator = sum.getDenominator();
    return sum;
}

/*
 * Operator ++ (example : my_rat++)
 * @param dummy parameter (int)
 * @return same Rat (Rat)
 */
Rat Rat::operator++(const int r) {
    Rat current(numerator, denominator);
    numerator+=denominator;
    this->simplifyRational();
    return current;
}


/*
 * Operator -- (example : my_rat--)
 * @param dummy parameter (int)
 * @return same Rat (Rat)
 */
Rat Rat::operator--(const int r) {
    Rat current(numerator, denominator);
    numerator-=denominator;
    return current;
}

/*
 * Operator -- (example : --my_rat)
 * @return Rat - 1 (Rat)
 */
Rat Rat::operator--() {
    Rat soustraction = *(this)-Rat(1,1);
    numerator = soustraction.getNumerator();
    denominator = soustraction.getDenominator();
    return soustraction;
}

/*
 * Operator == (example : rat_a == rat_b)
 * @param r (Rat)
 * @return isEqual (bool)
 */
bool Rat::operator==(const Rat& r) const {
    return (numerator == r.getNumerator() && denominator == r.getDenominator());
}

/*
 * Operator == (example : rat_a == int_variable)
 * @param r (int)
 * @return isEqual (bool)
 */
bool Rat::operator==(const int r) const {
    return static_cast<double>(*(this)) == (double)r;
}

/*
 * Operator != (example : rat_a != rat_b)
 * @param r (Rat)
 * @return isNotEqual (bool)
 */
bool Rat::operator!=(const Rat& r) const {
    return !(*(this) == r);
}

/*
 * Operator < (example : rat_a != rat_b)
 * @param r (Rat)
 * @return isSmallerThan (bool)
 */
bool Rat::operator<(const Rat& r) const{
    int num_a = numerator * r.getDenominator();
    int num_b = denominator * r.getNumerator();
    return num_a < num_b;
}

/*
 * Operator > (example : rat_a > rat_b)
 * @param r (Rat)
 * @return isGreaterThan (bool)
 */
bool Rat::operator>(const Rat& r) const{
    int num_a = numerator * r.getDenominator();
    int num_b = denominator * r.getNumerator();
    return num_a > num_b;
}

/*
 * Operator >= (example : rat_a >= rat_b)
 * @param r (Rat)
 * @return isGreaterOrEqualThan (bool)
 */
bool Rat::operator>=(const Rat& r) const{
    return (*(this) == r) || (*(this) > r);
}

/*
 * Operator <= (example : rat_a <= rat_b)
 * @param r (Rat)
 * @return isSmallerrOrEqualThan (bool)
 */
bool Rat::operator<=(const Rat& r) const{
    return (*(this) == r) || (*(this) < r);
}

/*
 * Convert Rat to float
 * @return float value of Rat (float)
 */
float Rat::toFloat() const {
    return (float) numerator / (float) denominator;
}


/*
 * Explicit Transtyping Rat to double
 * @return double value of Rat (double)
 */
Rat::operator double() const{
    return (double) numerator / (double) denominator;
}


/*
 * Print a Rat to stream
 * @param s (std::ostream&)
 * @param r (const Rat r)
 * @return stream (ostream)
 */
std::ostream& operator<<(std::ostream& s, const Rat r) {
    if (r.getNumerator() == 0 && r.getDenominator() == 1){
        s << "0 / 0";
    } else {
        s << r.getNumerator() << " / " << r.getDenominator();
    }

    return s;
}


/*
 * Simplify a rational number
 * @return void
 */
void Rat::simplifyRational() {
    int gcd = std::gcd(numerator, denominator);
    numerator = numerator / gcd;
    denominator = denominator / gcd;
}

/*
 * Adjust the sign of a rational number (+ or -)
 * @return void
 */
void Rat::adjustSign() {
    if (denominator < 0){
        numerator = -numerator;
        denominator = -denominator;
    }
}