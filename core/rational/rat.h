#ifndef RATIONAL_RAT_H
#define RATIONAL_RAT_H

#include <iostream>

class Rat
{
public:
    Rat();
    Rat(int num, int den=1);
    Rat(const double value, const int accuracy=3);

    int getNumerator() const;
    int getDenominator() const;

    Rat operator+() const;    // +Rat
    Rat operator-() const;    // -Rat

    Rat operator+(const Rat& operand) const;  // Rat + Rat
    Rat operator-(const Rat& operand) const;  // Rat - Rat
    Rat operator*(const Rat& operand) const;  // Rat * Rat
    Rat operator/(const Rat& operand) const;  // Rat / Rat

    Rat operator+=(const Rat& operand);   // Rat+=Rat
    Rat operator-=(const Rat& operand);   // Rat-=Rat

    Rat operator++();              // ++Rat  prefix
    Rat operator++(const int r);         // Rat++  postfix
    Rat operator--();              // --Rat
    Rat operator--(const int r);         // Rat--

    bool operator==(const Rat& r) const;        // Rat == Rat
    bool operator==(const int r) const;
    bool operator!=(const Rat& r) const;        // Rat != Rat
    bool operator<(const Rat& r) const;         // Rat < Rat
    bool operator>(const Rat& r) const;         // Rat > Rat
    bool operator<=(const Rat& r) const;        // Rat <= Rat
    bool operator>=(const Rat& r) const;        // Rat >= Rat

    float toFloat() const;

    operator double() const;

    friend std::ostream& operator<<(std::ostream& s, const Rat r);

private:

    void simplifyRational();
    void adjustSign();

    int numerator;
    int denominator;
};
#endif //RATIONAL_RAT_H
