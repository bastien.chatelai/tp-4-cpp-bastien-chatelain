#include "core/basic/matrix2d.h"

#include <iostream>
#include <chrono>

void benchmarkMultiplication(int size, int repetitions){
    Matrix2D<double> a(size, size);
    Matrix2D<double> b(size, size);

    a.random();
    b.random();
    auto start = std::chrono::system_clock::now();


    for(int i = 0;i < repetitions ; i++){
        Matrix2D<double> c = a * b;
    }

    auto passedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);

    std::cout << std::fixed << "Finished multiplication (" << size << ") in avg. " << (passedTime.count() / repetitions)<< "ms, total " << passedTime.count() << "ms" << std::endl;
}

void benchmarkAddition(int size, int repetitions){
    Matrix2D<double> a(size, size);
    Matrix2D<double> b(size, size);

    a.random();
    b.random();

    auto start = std::chrono::system_clock::now();

    for(int i = 0;i < repetitions ; i++){
        Matrix2D<double> c = a + b;
    }

    auto passedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);

    std::cout << std::fixed << "Finished addition (" << size << ") in avg. " << (passedTime.count() / repetitions)<< "ms, total " << passedTime.count() << "ms" << std::endl;
}

void benchmarkSubstraction(int size, int repetitions){
    Matrix2D<double> a(size, size);
    Matrix2D<double> b(size, size);

    a.random();
    b.random();

    auto start = std::chrono::system_clock::now();

    for(int i = 0;i < repetitions ; i++){
        Matrix2D<double> c = a - b;
    }

    auto passedTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);

    std::cout << std::fixed << "Finished substraction (" << size << ") in avg. " << (passedTime.count() / repetitions)<< "ms, total " << passedTime.count() << "ms" << std::endl;
}


int main() {

    benchmarkAddition(1000, 1000);
    benchmarkAddition(3000, 50);

    benchmarkSubstraction(1000, 1000);
    benchmarkSubstraction(3000, 50);

    benchmarkMultiplication(100, 100);
    benchmarkMultiplication(500, 3);
    benchmarkMultiplication(1000, 3);
    benchmarkMultiplication(2000, 1);

    return 0;
}